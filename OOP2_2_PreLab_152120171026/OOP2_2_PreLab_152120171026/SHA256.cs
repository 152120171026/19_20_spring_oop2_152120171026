﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Security.Cryptography;
using System.IO;
using System.Reflection;
using CsvHelper;
using System.Runtime.CompilerServices;

namespace OOP2_2_PreLab_152120171026
{
    class sha256
    {
        public static string ComputeSha256Hash(string rawData) // Password encrypt function
        {
            using (SHA256 sha256Hash = SHA256.Create()) // Create a SHA256   
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData)); // ComputeHash - returns byte array  
                StringBuilder builder = new StringBuilder(); // Convert byte array to a string   
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
    }
}
