﻿namespace OOP2_2_PreLab_152120171026
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLogout = new System.Windows.Forms.Button();
            this.psvExport = new System.Windows.Forms.Button();
            this.btnPhonebook = new System.Windows.Forms.Button();
            this.notes_button = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SalaryCalculator = new System.Windows.Forms.Button();
            this.profile1 = new OOP2_2_PreLab_152120171026.profile();
            this.SuspendLayout();
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(26)))), ((int)(((byte)(26)))));
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnLogout.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnLogout.Location = new System.Drawing.Point(9, 412);
            this.btnLogout.Margin = new System.Windows.Forms.Padding(0);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(142, 29);
            this.btnLogout.TabIndex = 10;
            this.btnLogout.Text = "Logout";
            this.btnLogout.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnLogout.UseVisualStyleBackColor = false;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // psvExport
            // 
            this.psvExport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(26)))), ((int)(((byte)(26)))));
            this.psvExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.psvExport.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.psvExport.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.psvExport.Location = new System.Drawing.Point(9, 383);
            this.psvExport.Margin = new System.Windows.Forms.Padding(0);
            this.psvExport.Name = "psvExport";
            this.psvExport.Size = new System.Drawing.Size(142, 29);
            this.psvExport.TabIndex = 11;
            this.psvExport.Text = "Export Users";
            this.psvExport.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.psvExport.UseVisualStyleBackColor = false;
            this.psvExport.Click += new System.EventHandler(this.psvExport_Click);
            // 
            // btnPhonebook
            // 
            this.btnPhonebook.Location = new System.Drawing.Point(9, 12);
            this.btnPhonebook.Name = "btnPhonebook";
            this.btnPhonebook.Size = new System.Drawing.Size(142, 23);
            this.btnPhonebook.TabIndex = 12;
            this.btnPhonebook.Text = "PhoneBook";
            this.btnPhonebook.UseVisualStyleBackColor = true;
            this.btnPhonebook.Click += new System.EventHandler(this.BtnPhonebook_Click);
            // 
            // notes_button
            // 
            this.notes_button.Location = new System.Drawing.Point(9, 41);
            this.notes_button.Name = "notes_button";
            this.notes_button.Size = new System.Drawing.Size(142, 24);
            this.notes_button.TabIndex = 13;
            this.notes_button.Text = "Notes";
            this.notes_button.UseVisualStyleBackColor = true;
            this.notes_button.Click += new System.EventHandler(this.button1_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(26)))), ((int)(((byte)(26)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button1.Location = new System.Drawing.Point(9, 103);
            this.button1.Margin = new System.Windows.Forms.Padding(0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(142, 29);
            this.button1.TabIndex = 14;
            this.button1.Text = "Profili Görüntüle";
            this.button1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // SalaryCalculator
            // 
            this.SalaryCalculator.Location = new System.Drawing.Point(9, 71);
            this.SalaryCalculator.Name = "SalaryCalculator";
            this.SalaryCalculator.Size = new System.Drawing.Size(142, 29);
            this.SalaryCalculator.TabIndex = 16;
            this.SalaryCalculator.Text = "Salary Calculator";
            this.SalaryCalculator.UseVisualStyleBackColor = true;
            this.SalaryCalculator.Click += new System.EventHandler(this.SalaryCalculator_Click);
            // 
            // profile1
            // 
            this.profile1.Location = new System.Drawing.Point(161, 15);
            this.profile1.Name = "profile1";
            this.profile1.Size = new System.Drawing.Size(631, 426);
            this.profile1.TabIndex = 15;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.SalaryCalculator);
            this.Controls.Add(this.profile1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.notes_button);
            this.Controls.Add(this.btnPhonebook);
            this.Controls.Add(this.psvExport);
            this.Controls.Add(this.btnLogout);
            this.Name = "Main";
            this.Text = "Main";
            this.Load += new System.EventHandler(this.Main_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Button psvExport;
        private System.Windows.Forms.Button btnPhonebook;
        private System.Windows.Forms.Button notes_button;
        private System.Windows.Forms.Button button1;
        private profile profile1;
        private System.Windows.Forms.Button SalaryCalculator;
    }
}