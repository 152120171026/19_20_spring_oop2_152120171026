﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.CodeDom.Compiler;
using System.IO;
using System.Drawing.Imaging;

namespace OOP2_2_PreLab_152120171026
{
    public partial class profile : UserControl
    {
        OpenFileDialog file = new OpenFileDialog();
        public static UserClass temp = new UserClass("", "", "", "", "", "", "", "","");
        private bool boxChanged = false; // to check password textbox changes 
        public string base64photo;

        public profile()
        {
            InitializeComponent();
        }

        private void profile_Load(object sender, EventArgs e)
        {
            base64photo = temp.base64photo;
            txtName.Text = Login.temp.name;
            txtPass.Text = "******";
            txtAdress.Text = Login.temp.adress;
            txtEmail.Text = Login.temp.email;
            txtPhone.Text = Login.temp.phoneNumber;
            txtSurname.Text = Login.temp.surname;
            txtSalary.Text = Login.loggedInUser.salary;
            profilePictureBox.Image = String2Image(Login.temp.base64photo);
            boxChanged = false;
            profilePictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
        }



        private void btnEdit_Click(object sender, EventArgs e)
        {
            btnKabul.Visible = true;
            btnIptal.Visible = true;
            btnPhoto.Visible = true;
            txtName.Enabled = true;
            txtPass.Enabled = true;
            txtAdress.Enabled = true;
            txtEmail.Enabled = true;
            txtPhone.Enabled = true;
            txtSurname.Enabled = true;
            txtSalary.Enabled = true;
        }

        private void btnKabul_Click(object sender, EventArgs e)
        {
            File.WriteAllText(pathfinder.PathFinder("users.csv"), "");
            writeToCsv();
            btnKabul.Visible = false;
            btnIptal.Visible = false;
            btnPhoto.Visible = false;
            txtName.Enabled = false;
            txtPass.Enabled = false;
            txtAdress.Enabled = false;
            txtEmail.Enabled = false;
            txtPhone.Enabled = false;
            txtSurname.Enabled = false;
            txtSalary.Enabled = false;
        }

        private void txtPass_TextChanged(object sender, EventArgs e)
        {
            this.boxChanged = true;
        }

        //public string ImageToBase64(Image image, System.Drawing.Imaging.ImageFormat format)
        //{
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        // Convert Image to byte[]
        //        image.Save(ms, format);
        //        byte[] imageBytes = ms.ToArray();

        //        // Convert byte[] to Base64 String
        //        string base64String = Convert.ToBase64String(imageBytes);
        //        return base64String;
        //    }
        //}

        public string Fileto64(string filepath)
        {
            try
            {
                byte[] byts = System.IO.File.ReadAllBytes(filepath);
                return Convert.ToBase64String(byts);
            }
            catch { return null; }
        }

        public Image String2Image(string p)
        {
            try
            {
                byte[] bytes = Convert.FromBase64String(p);


                Image image;
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    image = Image.FromStream(ms);
                }

                return image;
            }
            catch { return null; }
        }

        private void btnPhoto_Click(object sender, EventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            file.ShowDialog();
            string filePath = file.FileName;
            profilePictureBox.ImageLocation = filePath;
            base64photo = Fileto64(filePath);
            
        }

        private void writeToCsv()
        {
            File.WriteAllText(pathfinder.PathFinder("users.csv"), "");
            for (int i = 0; i < Login.varName.Count; i++)
            {
                temp = (UserClass)Login.varName[i];
                if (Login.loggedInUser.name == temp.name && boxChanged)
                {
                    File.AppendAllText(pathfinder.PathFinder("users.csv"),
                        txtName.Text + ","
                        + sha256.ComputeSha256Hash(txtPass.Text) + ","
                        + Login.loggedInUser.userRole + ","
                        + txtSurname.Text + ","
                        + txtPhone.Text + ","
                        + txtAdress.Text + ","
                        + txtEmail.Text + ","
                        + txtSalary.Text + ","
                        + base64photo
                        + Environment.NewLine);
                }

                else if (Login.loggedInUser.name == temp.name && !boxChanged)
                {
                    File.AppendAllText(pathfinder.PathFinder("users.csv"),
                        txtName.Text + ","
                        + Login.loggedInUser.hashedData + ","
                        + Login.loggedInUser.userRole + ","
                        + txtSurname.Text + ","
                        + txtPhone.Text + ","
                        + txtAdress.Text + ","
                        + txtEmail.Text + ","
                        + txtSalary.Text + ","
                        + base64photo
                        + Environment.NewLine);
                }
                else
                    File.AppendAllText(pathfinder.PathFinder("users.csv"),
                        temp.name + ","
                        + temp.hashedData + ","
                        + temp.userRole + ","
                        + temp.surname + ","
                        + temp.phoneNumber + ","
                        + temp.adress + ","
                        + temp.email + ","
                        + txtSalary.Text + ","
                        + base64photo
                        + Environment.NewLine);
            }
        }

        private void profile_Enter(object sender, EventArgs e)
        {
            txtSalary.Text = Login.loggedInUser.salary;
        }
    }
}

//profilePictureBox.SizeMode = PictureBoxSizeMode.StretchImage;