﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.CodeDom.Compiler;
using System.IO;
using System.Drawing.Imaging;

namespace OOP2_2_PreLab_152120171026
{
    public partial class SalaryCalculate : Form
    {
        OpenFileDialog file = new OpenFileDialog();
        public static UserClass temp = new UserClass("", "", "", "", "", "", "", "", "");
        public static double a;
        public static double b;
        public static double c;
        public static double d;
        public static double ee;
        public static double f;
        public static double g;
        public static double h;
        public static double j;
        public static double UserSalary;
        public SalaryCalculate()
        {
            InitializeComponent();
        }

        private void SalaryCalculate_Load(object sender, EventArgs e)
        {
            a = 0; b = 0; c = 0; d = 0;
            ee = 0; f = 0; g = 0; h = 0;
            j = 1;
            label6.Visible = false;
            comboBox6.Visible = false;
        }



        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)//Deneyim combobox
        {
            if (comboBox1.SelectedIndex == 0) { a = 0.60; }
            if (comboBox1.SelectedIndex == 1) { a = 1; }
            if (comboBox1.SelectedIndex == 2) { a = 1.20; }
            if (comboBox1.SelectedIndex == 3) { a = 1.35; }
            if (comboBox1.SelectedIndex == 4) { a = 1.50; }
        }

        private void ComboBox2_SelectedIndexChanged(object sender, EventArgs e)//Yaşanılan il combobox
        {
            if (comboBox2.SelectedIndex == 0) { b = 0.15; }
            if (comboBox2.SelectedIndex == 1) { b = 0.10; }
            if (comboBox2.SelectedIndex == 2) { b = 0.10; }
            if (comboBox2.SelectedIndex == 3) { b = 0.05; }
            if (comboBox2.SelectedIndex == 4) { b = 0.05; }
            if (comboBox2.SelectedIndex == 5) { b = 0.03; }
            if (comboBox2.SelectedIndex == 6) { b = 0.03; }
            if (comboBox2.SelectedIndex == 7) { b = 0.03; }
            if (comboBox2.SelectedIndex == 8) { b = 0.03; }
            if (comboBox2.SelectedIndex == 9) { b = 0.03; }
            if (comboBox2.SelectedIndex == 10) { b = 0.03; }
            if (comboBox2.SelectedIndex == 11) { b = 0.03; }
        }

        private void ComboBox3_SelectedIndexChanged(object sender, EventArgs e)//Üst Öğrenim
        {
            if (comboBox3.SelectedIndex == 0) { c = 0.10; }
            if (comboBox3.SelectedIndex == 1) { c = 0.30; }
            if (comboBox3.SelectedIndex == 2) { c = 0.35; }
            if (comboBox3.SelectedIndex == 3) { c = 0.05; }
            if (comboBox3.SelectedIndex == 4) { c = 0.15; }
        }

        private void ComboBox4_SelectedIndexChanged(object sender, EventArgs e)//Yabancı dil bilgisi
        {
            if (comboBox4.SelectedIndex == 0) { d = 0.20; }
            if (comboBox4.SelectedIndex == 1) { d = 0.20; }
        }
        private void ComboBox7_SelectedIndexChanged(object sender, EventArgs e)//Bilinen diğer yabancı dil sayısı
        {
            if (comboBox7.SelectedIndex == 0) { ee = 0.05; }
            if (comboBox7.SelectedIndex == 1) { ee = 0.10; }
            if (comboBox7.SelectedIndex == 2) { ee = 0.15; }
            if (comboBox7.SelectedIndex == 3) { ee = 0.20; }
            if (comboBox7.SelectedIndex == 4) { ee = 0.25; }
            if (comboBox7.SelectedIndex == 5) { ee = 0.30; }
            if (comboBox7.SelectedIndex == 6) { ee = 0.35; }
        }

        private void ComboBox5_SelectedIndexChanged(object sender, EventArgs e)//Yöneticilik Görevi
        {
            if (comboBox5.SelectedIndex == 0) { f = 0.50; }
            if (comboBox5.SelectedIndex == 1) { f = 0.75; }
            if (comboBox5.SelectedIndex == 2) { f = 0.85; }
            if (comboBox5.SelectedIndex == 3) { f = 1; }
            if (comboBox5.SelectedIndex == 4) { f = 0.40; }
            if (comboBox5.SelectedIndex == 5) { f = 0.60; }
        }

        private void BtnCalculateSalary_Click(object sender, EventArgs e)
        {
            double totalsalary;
            double totalkatsayi;

            totalkatsayi = a + b + c + d + ee + f + g + h + 1;
            totalsalary = (totalkatsayi * 4500) / j;
            SalaryLabel.Text = ("Your salary should be " + totalsalary.ToString());
            UserSalary = totalsalary;
            Login.loggedInUser.salary = UserSalary.ToString();
            writeToCsv();
        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
                h = 0.20;
            else if (checkBox1.Checked == false)
                h = 0;
        }

        private void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                label6.Visible = true;
                this.comboBox6.Visible = true;
            }
            else if (checkBox2.CheckState == CheckState.Unchecked)
            {
                label6.Visible = false;
                comboBox6.Visible = false;
            }
        }

        private void ComboBox6_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox6.SelectedIndex == 0) { g = 0.20; }
            if (comboBox6.SelectedIndex == 1) { g = 0.30; }
            if (comboBox6.SelectedIndex == 2) { g = 0.40; }
            if (comboBox6.SelectedIndex == 3) { g = 0.40; }
            if (comboBox6.SelectedIndex == 4) { g = 0.60; }
            if (comboBox6.SelectedIndex == 5) { g = 0.80; }
            if (comboBox6.SelectedIndex == 6) { g = 0.50; }
            if (comboBox6.SelectedIndex == 7) { g = 0.60; }
            if (comboBox6.SelectedIndex == 8) { g = 0.70; }
        }

        private void CheckBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.CheckState == CheckState.Checked)
            {
                j = 2;
            }
            else if (checkBox3.CheckState == CheckState.Unchecked)
            {
                j = 1;
            }
        }
        private void writeToCsv()
        {
            File.WriteAllText(pathfinder.PathFinder("users.csv"), "");
            for (int i = 0; i < Login.varName.Count; i++)
            {
                temp = (UserClass)Login.varName[i];
                if (Login.loggedInUser.name == temp.name)
                {
                    File.AppendAllText(pathfinder.PathFinder("users.csv"),
                        Login.loggedInUser.name + ","
                        + Login.loggedInUser.hashedData + ","
                        + Login.loggedInUser.userRole + ","
                        + Login.loggedInUser.surname + ","
                        + Login.loggedInUser.phoneNumber + ","
                        + Login.loggedInUser.adress + ","
                        + Login.loggedInUser.email + ","
                        + Login.loggedInUser.salary + ","
                        + Login.loggedInUser.base64photo
                        + Environment.NewLine);
                }
                else
                    File.AppendAllText(pathfinder.PathFinder("users.csv"),
                        temp.name + ","
                        + temp.hashedData + ","
                        + temp.userRole + ","
                        + temp.surname + ","
                        + temp.phoneNumber + ","
                        + temp.adress + ","
                        + temp.email + ","
                        + temp.salary + ","
                        + temp.base64photo
                        + Environment.NewLine);
            }
        }
    }
}