﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Security.Cryptography;
using System.IO;
using System.Reflection;

namespace OOP2_2_PreLab_152120171026
{
    public partial class Main : Form
    {
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;

        public Main()
        {
            InitializeComponent();

        }

        private void Main_Load(object sender, EventArgs e)
        {

        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            OOP2_2_PreLab_152120171026.Properties.Settings.Default.userName = "";
            OOP2_2_PreLab_152120171026.Properties.Settings.Default.password = "";
            OOP2_2_PreLab_152120171026.Properties.Settings.Default.Save();
            Application.Exit();
        }

        private void psvExport_Click(object sender, EventArgs e)
        {
            // PSV export

            using (this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog())
            {
                saveFileDialog1.Title = "Export Users";
                saveFileDialog1.CheckFileExists = false;
                saveFileDialog1.CheckPathExists = true;
                saveFileDialog1.DefaultExt = "psv";
                saveFileDialog1.FilterIndex = 2;
                saveFileDialog1.RestoreDirectory = true;
                saveFileDialog1.FileName = "users.psv";
                string path = System.IO.Path.GetDirectoryName(saveFileDialog1.FileName);

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    using (var reader = new StreamReader(pathfinder.PathFinder("users.csv")))
                    {
                        while (!reader.EndOfStream)
                        {
                            var line = reader.ReadLine();
                            var values = line.Split(',');
                            File.AppendAllText(saveFileDialog1.FileName,
                                values[0] + "|" + sha256.ComputeSha256Hash(values[1]) + Environment.NewLine);
                        }
                    }
                }


            }
        }

        private void BtnPhonebook_Click(object sender, EventArgs e)
        {
            this.Hide();
            PhoneBook p = new PhoneBook();
            p.ShowDialog();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Notes n = new Notes();
            n.ShowDialog();
            this.Close();
        }

        private void SalaryCalculator_Click(object sender, EventArgs e)
        {
            SalaryCalculate A6 = new SalaryCalculate();
            A6.Show();
        }
    }
}
