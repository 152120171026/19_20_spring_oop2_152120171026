﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP2_2_PreLab_152120171026
{
    public partial class PhoneBook : Form
    {
        public PhoneBook()
        {
            InitializeComponent();
        }

        private void BtnNew_Click(object sender, EventArgs e)
        {
            try
            {
                panel1.Enabled = true;
                App.PhoneBook.AddPhoneBookRow(App.PhoneBook.NewPhoneBookRow());
                phoneBookBindingSource.MoveLast();
                txtFirstName.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                App.PhoneBook.RejectChanges();
            }

        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            panel1.Enabled = true;
            txtFirstName.Focus();

        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            phoneBookBindingSource.ResetBindings(false);
            panel1.Enabled = false;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (IsValidEmail(txtEmail.Text) == true)
            {
                try
                {
                    phoneBookBindingSource.EndEdit();
                    App.PhoneBook.AcceptChanges();
                    App.PhoneBook.WriteXml(string.Format("{0}//data.dat", Application.StartupPath));
                    panel1.Enabled = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    App.PhoneBook.RejectChanges();
                }
            }
            else
                MessageBox.Show("Please Enter a valid e-mail", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }

        static AppData db;

        protected static AppData App
        {
            get
            {
                if (db == null)
                    db = new AppData();
                return db;
            }
        }
        private void PhoneBook_Load(object sender, EventArgs e)
        {
            string fileName = string.Format("{0}//data.dat", Application.StartupPath);
            if (File.Exists(fileName))
                App.PhoneBook.ReadXml(fileName);
            phoneBookBindingSource.DataSource = App.PhoneBook;
            panel1.Enabled = false;
        }

        private void DataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (MessageBox.Show("Are you sure you want to delete this record?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    phoneBookBindingSource.RemoveCurrent();
            }
        }

        private void TxtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            /* if (!string.IsNullOrEmpty(txtSearch.Text))
             {
                 var query = from o in App.PhoneBook
                             where o.First_Name.Contains(txtSearch.Text) || o.Email == txtSearch.Text || o.Last_Name.Contains(txtSearch.Text) || o.Phone_Number == txtSearch.Text 
                             select o;
                 dataGridView.DataSource = query.ToList();
             }
             else
                 dataGridView.DataSource = phoneBookBindingSource;*/
            try
            {
                dataGridView.ClearSelection();
                if (txtSearch.Text.Length > 0)
                {
                    foreach (DataGridViewRow row in dataGridView.Rows)
                    {
                        for (int i = 0; i < row.Cells.Count; i++)
                        {
                            if (row.Cells[i].Value != null && row.Cells[i].Value.ToString().ToLower().Contains(txtSearch.Text.ToLower()))
                            {
                                int rowIndex = row.Index;
                                dataGridView.Rows[rowIndex].Selected = true;
                                break;
                            }
                        }

                    }
                }
                else
                {
                    dataGridView.ClearSelection();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void TxtPhoneNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void TxtFirstName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void TxtLastName_KeyPress(object sender, KeyPressEventArgs e)
        {
                e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);           
        }
        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private void BtnExportCSV_Click(object sender, EventArgs e)
        {
            string csv = string.Empty;
            csv = csv + "sep=," + "\r\n";
            foreach (DataGridViewColumn column in dataGridView.Columns)
            {
                csv += column.HeaderText + ',';
            }
            csv += "\r\n";
            foreach (DataGridViewRow row in dataGridView.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    csv += cell.Value.ToString().Replace(",", " ") + ',';
                }
                csv += "\r\n";
            }
            string folderPath = pathfinder.PathFinder("PhoneBook.csv");
            File.WriteAllText(folderPath, csv);

            MessageBox.Show("Phonebook exported to : " + folderPath, "Succesful", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }

}