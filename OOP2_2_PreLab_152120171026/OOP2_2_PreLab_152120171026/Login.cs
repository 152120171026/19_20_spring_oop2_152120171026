﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Security.Cryptography;
using System.IO;
using System.Reflection;
using CsvHelper;
using System.Runtime.CompilerServices;

namespace OOP2_2_PreLab_152120171026
{
    public partial class Login : Form
    {
       

        public static ArrayList varName;
        public static UserClass temp = new UserClass("", "", "", "", "", "", "", "","");
        public static UserClass loggedInUser = new UserClass("", "", "", "", "", "", "", "","");
        public Login()
        {
            InitializeComponent();
            this.AcceptButton = this.btnLogin; // key "enter"s default function is login
            varName = new ArrayList();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if(OOP2_2_PreLab_152120171026.Properties.Settings.Default.userName != "")// remember me checkbox status 
            {
                checkBoxRemember.Checked= true;
            }

            txtName.Text = OOP2_2_PreLab_152120171026.Properties.Settings.Default.userName; // fill name textbox
            txtPass.Text = OOP2_2_PreLab_152120171026.Properties.Settings.Default.password; // fill password textbox

            using (var reader = new StreamReader(pathfinder.PathFinder("users.csv"))) //read users.csv
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    UserClass createUser = new UserClass(values[0], values[1], values[2], values[3], values[4], values[5], values[6], values[7],values[8]);
                    varName.Add(createUser);
                }
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < varName.Count; i++)
            {
                string hash = sha256.ComputeSha256Hash(txtPass.Text);
                temp = (UserClass)varName[i];
                if (txtName.Text == temp.name && hash == temp.hashedData)
                {
                    if (checkBoxRemember.Checked)
                    {
                        OOP2_2_PreLab_152120171026.Properties.Settings.Default.userName = txtName.Text;
                        OOP2_2_PreLab_152120171026.Properties.Settings.Default.password = txtPass.Text;
                        OOP2_2_PreLab_152120171026.Properties.Settings.Default.Save();
                    }

                    lbllogin.Text = "Access Granted!";
                    lbllogin.ForeColor = Color.Green;
                    timer1.Enabled = true;
                    loggedInUser = temp;
                    break;
                }

                else
                {
                    lbllogin.Text = "Access Rejected!";
                    lbllogin.ForeColor = Color.Red;
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e) //wait 3 secodns before login
        {
            panel5.Width += 3;
            if(panel5.Width >= 300)
            {
                timer1.Stop();
                Main main = new Main();
                main.Show();
                this.Hide();
            }
        }

        private void btnRgstr_Click(object sender, EventArgs e)
        {
            Register register = new Register();
            register.Show();
        }      
    }
}
