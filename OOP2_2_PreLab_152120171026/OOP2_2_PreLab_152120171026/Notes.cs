﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP2_2_PreLab_152120171026
{
    public partial class Notes : Form
    {
        DataTable table;
        int update_process = 0;

        public Notes()
        {
            InitializeComponent();
        }

        private void Notes_Load(object sender, EventArgs e)
        {
            table = new DataTable();
            table.Columns.Add("Notes", typeof(String));

            dataGridView1.DataSource = table;
            dataGridView1.Columns["Notes"].Width = 337;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void CreatenewNote_Click(object sender, EventArgs e)
        {
            notes_txtbox.Clear();
        }
        private void Savebttn_Click(object sender, EventArgs e)
        {
            table.Rows.Add(notes_txtbox.Text);
            notes_txtbox.Clear();
        }
        private void uploadbttn_Click(object sender, EventArgs e)
        {

            update_process = 1;
            if (update_process == 0)
            {
                table.Rows.Add(notes_txtbox.Text);
                notes_txtbox.Clear();
            }
            else
            {
                
                int index = dataGridView1.CurrentCell.RowIndex;
                if (index != -1){
                    table.Rows[index][0] = notes_txtbox.Text;
                    update_process = 0;
                    notes_txtbox.Clear();
                }
                
            }
            
        }

        private void deletebttn_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentCell != null)
            {
                int index = dataGridView1.CurrentCell.RowIndex;
                table.Rows[index].Delete();
            }
        }

        private void BttnExportCSV_Click(object sender, EventArgs e)
        {
            string csv = string.Empty;
            csv = csv + "sep=," + "\r\n";
            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {
                csv += column.HeaderText + ',';
            }
            csv += "\r\n";
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    csv += cell.Value.ToString().Replace(",", " ") + ',';
                }
                csv += "\r\n";
            }
            string folderPath = pathfinder.PathFinder("Notes.csv");
            File.WriteAllText(folderPath, csv);
            MessageBox.Show("Notes exported to : " + folderPath, "Succesful", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
    }
}
