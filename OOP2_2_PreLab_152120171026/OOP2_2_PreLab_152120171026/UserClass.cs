﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2_2_PreLab_152120171026
{
    public class UserClass // Users
    {
        public string name;
        public string hashedData; // encrypted password
        public string userRole; // User types: Admin, user, part-time-user
        public string surname;
        public string phoneNumber;
        public string adress;
        public string email;
        public string base64photo;
        public string salary;
       
        public UserClass(string name, string pass, string userRole, string surname, string phoneNumber, string adress, string email, string base64photo,string salary)
        {
            this.name = name;
            this.hashedData = pass;
            this.userRole = userRole;
            this.surname = surname;
            this.phoneNumber = phoneNumber;
            this.adress = adress;
            this.email = email;
            this.base64photo = base64photo;
            this.salary = salary;
        }

        public UserClass(string name, string pass, string userRole)
        {
            this.name = name;
            this.hashedData = pass;
            this.userRole = userRole;
        }
    };
}
