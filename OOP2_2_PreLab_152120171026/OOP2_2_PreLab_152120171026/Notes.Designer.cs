﻿namespace OOP2_2_PreLab_152120171026
{
    partial class Notes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.notes_txtbox = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.uploadbttn = new System.Windows.Forms.Button();
            this.deletebttn = new System.Windows.Forms.Button();
            this.CreatenewNote = new System.Windows.Forms.Button();
            this.Savebttn = new System.Windows.Forms.Button();
            this.BttnExportCSV = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Notes";
            // 
            // notes_txtbox
            // 
            this.notes_txtbox.Location = new System.Drawing.Point(13, 34);
            this.notes_txtbox.Multiline = true;
            this.notes_txtbox.Name = "notes_txtbox";
            this.notes_txtbox.Size = new System.Drawing.Size(360, 256);
            this.notes_txtbox.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(424, 34);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(327, 256);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // uploadbttn
            // 
            this.uploadbttn.Location = new System.Drawing.Point(253, 315);
            this.uploadbttn.Name = "uploadbttn";
            this.uploadbttn.Size = new System.Drawing.Size(75, 23);
            this.uploadbttn.TabIndex = 5;
            this.uploadbttn.Text = "Update";
            this.uploadbttn.UseVisualStyleBackColor = true;
            this.uploadbttn.Click += new System.EventHandler(this.uploadbttn_Click);
            // 
            // deletebttn
            // 
            this.deletebttn.Location = new System.Drawing.Point(361, 316);
            this.deletebttn.Name = "deletebttn";
            this.deletebttn.Size = new System.Drawing.Size(75, 23);
            this.deletebttn.TabIndex = 6;
            this.deletebttn.Text = "Delete";
            this.deletebttn.UseVisualStyleBackColor = true;
            this.deletebttn.Click += new System.EventHandler(this.deletebttn_Click);
            // 
            // CreatenewNote
            // 
            this.CreatenewNote.Location = new System.Drawing.Point(16, 316);
            this.CreatenewNote.Name = "CreatenewNote";
            this.CreatenewNote.Size = new System.Drawing.Size(75, 23);
            this.CreatenewNote.TabIndex = 7;
            this.CreatenewNote.Text = "New";
            this.CreatenewNote.UseVisualStyleBackColor = true;
            this.CreatenewNote.Click += new System.EventHandler(this.CreatenewNote_Click);
            // 
            // Savebttn
            // 
            this.Savebttn.Location = new System.Drawing.Point(125, 315);
            this.Savebttn.Name = "Savebttn";
            this.Savebttn.Size = new System.Drawing.Size(75, 23);
            this.Savebttn.TabIndex = 8;
            this.Savebttn.Text = "Save";
            this.Savebttn.UseVisualStyleBackColor = true;
            this.Savebttn.Click += new System.EventHandler(this.Savebttn_Click);
            // 
            // BttnExportCSV
            // 
            this.BttnExportCSV.Location = new System.Drawing.Point(466, 316);
            this.BttnExportCSV.Name = "BttnExportCSV";
            this.BttnExportCSV.Size = new System.Drawing.Size(94, 23);
            this.BttnExportCSV.TabIndex = 9;
            this.BttnExportCSV.Text = "ExportCSV";
            this.BttnExportCSV.UseVisualStyleBackColor = true;
            this.BttnExportCSV.Click += new System.EventHandler(this.BttnExportCSV_Click);
            // 
            // Notes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.BttnExportCSV);
            this.Controls.Add(this.Savebttn);
            this.Controls.Add(this.CreatenewNote);
            this.Controls.Add(this.deletebttn);
            this.Controls.Add(this.uploadbttn);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.notes_txtbox);
            this.Controls.Add(this.label1);
            this.Name = "Notes";
            this.Text = "Notes";
            this.Load += new System.EventHandler(this.Notes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox notes_txtbox;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button uploadbttn;
        private System.Windows.Forms.Button deletebttn;
        private System.Windows.Forms.Button CreatenewNote;
        private System.Windows.Forms.Button Savebttn;
        private System.Windows.Forms.Button BttnExportCSV;
    }
}