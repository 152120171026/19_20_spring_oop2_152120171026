﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Security.Cryptography;
using System.IO;
using System.Reflection;
using CsvHelper;
using System.Runtime.CompilerServices;

namespace OOP2_2_PreLab_152120171026
{
    class pathfinder
    {
        public static string PathFinder(string fn)
        {
            string FileName = fn;
            var outPutDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase); // Relative to the exe. Path to Debug file.
            var csvPath = Path.Combine(outPutDirectory, FileName); // Csv file directory var.
            string csv_path = new Uri(csvPath).LocalPath;  // Path to the csv file.
            if (File.Exists(csv_path) == false)
            {
                File.Create(csv_path).Close();
            }

            return csv_path;
        }

    }
}
